import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import Fade from '@material-ui/core/Fade';
import Slide from '@material-ui/core/Slide';
import Grow from '@material-ui/core/Grow';

function SlideTransition(props) {
    return <Slide {...props} message={'Hello World'} direction='up' />;
}

function GrowTransition(props) {
    return <Grow {...props}   message={'Hello World'}/>;
}

export default function TransitionsSnackbar({...props}) {
    const handleClose = () => {
        props.close()
    };
    return (
        <div>
            <Snackbar
                open={props.open}
                onClose={handleClose}
                TransitionComponent={Fade}
                message={props.message}
                autoHideDuration={1000}
            />
        </div>
    );
}
